let users = `[
		{
		      "id": 1,
		      "First Name": "Maria",
		      "Last Name": "De la Cruz",
		      "email": "row@gmail.com",
		      "Password": "89A45",
		      "Is Admin": false,
		      "mobile number": "09000301111" 
		   },
		  	{
		      "id": 2,
		      "First Name": "George",
		      "Last Name": "De la Cruz",
		      "email": "rus@gmail.com",
		      "Password": "89098",
		      "Is Admin": false,
		      "mobile number": "09000001121" 
		   },
		    {
		      "id": 3,
		      "First Name": "Santi",
		      "Last Name": "De la Cruz",
		      "email": "ree@gmail.com",
		      "Password": "opo90",
		      "Is Admin": false,
		      "mobile number": "0900000656" 
		   }
	]`;

	console.log(JSON.parse(users));

	let orders = `[
		{
		      "userID": 1,
		      "Transaction Date": "Dec 12",
		      "Status": "Delivered",
		      "Total": "500.00" 
		   },
		  	{
		      "userID": 2,
		      "Transaction Date": "Jan 14",
		      "Status": "Order received",
		      "Total": "800.00" 
		   },
		    {
		      "userID": 3,
		      "Transaction Date": "Jun 3",
		      "Status": "Cancelled",
		      "Total": "0.00" 
		   }
	]`;

	console.log(JSON.parse(orders));

	let products = `[
			{
			      "Name": "MAC book",
			      "Description": "White",
			      "Price": "60,000",
			      "Stocks": "7",
			      "Is Active": true, 
			      "SKU": "YYY"
			   },
			  	{
			      "Name": "Dell",
			      "Description": "Black",
			      "Price": "40,000",
			      "Stocks": "9",
			      "Is Active": true, 
			      "SKU": "YYY"
			   },
			    {
			      "Name": "Samsung",
			      "Description": "White",
			      "Price": "50,000",
			      "Stocks": "10",
			      "Is Active": true,
			      "SKU": "YYY"
			   }
		]`;

		console.log(JSON.parse(products));

		let OrderProducts = `[
			{
			      "OrderID": "B2",
			      "ProductID": "XYZ123",
			      "Quantity": "56",
			      "Price": "5000.00",
			      "Subtotal": "5000.00" 
			   },
			  	{
			      "OrderID": "C4",
			      "ProductID": "MNO890",
			      "Quantity": "9",
			      "Price": "8000.00",
			      "Subtotal": "8000.00" 
			   },
			    {
			      "OrderID": "D2",
			      "ProductID": "HHHY12",
			      "Quantity": "10",
			      "Price": "4000.00",
			      "Subtotal": "4000.00" 
			   }
		]`;

		console.log(JSON.parse(OrderProducts));
